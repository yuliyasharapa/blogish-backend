const db = require('../database/models');
const isImageUrl = require('is-image-url');

let postsController = {
    listAll: async (req,res) => {
        try {
            const posts = await db.Post.findAll({
                include: [
                    {model: db.Category, as: "category"},
                ],
                order: [['createdAt', 'DESC']]
            });
            res.json(posts);
        } catch (error) {
            res.status(500).json({message : error.message});
        }
    },
    getById: async (req,res) => {
        try {
            const post = await db.Post.findByPk(req.params.id, {include: [{model: db.Category, as: "category"}]} );
            res.json(post);
        } catch (error) {
            res.status(500).json({message : error.message});
        }
    },
    create: async (req, res) => {
        try {
            const createdAt = new Date();
            const {title, body, image, categoryId} = req.body;

            if (!isImageUrl(image))
            return res.status(400).json({ msg: "The URL is not an image" });

            const newPost = await db.Post.create({
                title,
                body,
                image,
                createdAt,
                categoryId,
            });
            res.json(newPost);
        } catch (error) {
            res.status(500).json({message : error.message});
        }
    },
    edit: async (req,res) => {
        try {
            const {title, body, image, createdAt, categoryId} = req.body;
            const post = await db.Post.update({
                title,
                body,
                image,
                createdAt,
                categoryId,
            },
            { where:
                {id:req.params.id},
                returning: true,
                plain: true
            })
            if (!post) return res.status(404).json({ msg: "No post was found" });
            res.json(post)
        } catch (error) {
            res.status(500).json({ error: err.message });
        }
    },
    remove: async (req,res) => {
        try {
            await db.Post.destroy(
                { where: 
                    {id:req.params.id}
                });
            res.send("ok");
        } catch (error) {
            res.status(500).json({message : error.message});
        }
    },
}

module.exports = postsController;