module.exports = (sequelize, DataTypes) => {
    let alias = "Post";
    let cols = {
        id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
        title: {
			type: DataTypes.STRING,
			allowNull: false
		},
        body: {
			type: DataTypes.STRING,
			allowNull: false
		},
        image: {
			type: DataTypes.STRING,
			allowNull: true
		},
        createdAt: {
			type: DataTypes.DATE,
			allowNull: true
		},
    };
    let config = {
        tableName: 'posts',
        timestamps: false
    }
    
    const Post = sequelize.define(alias, cols, config) ;

    Post.associate = (models) => {
        Post.belongsTo(models.Category, {
            as: "category"
        });
    }


    return Post;
}