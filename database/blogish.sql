-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: blogish
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'travel'),(2,'food'),(3,'psychology'),(4,'news');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `body` varchar(2000) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `categoryId` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_blog_category_idx` (`categoryId`),
  CONSTRAINT `fk_blog_category` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (4,'How 20 years of conflict have reshaped Afghanistan’s capital and life in it','At the center of Kabul, a city of traditional bazaars and tattered shopping malls, horse-drawn carts and crumbling streets thronged with automobile traffic, lies a heavily fortified district that is a mystery to most Afghans.\n\nWhat was once a cluster of key offices and compounds has evolved into a 21st-century fortress encircled by blast walls, checkpoints and security cameras, creating what for many is an impenetrable urban void known as the Green Zone.\n\nFortifications expanded rapidly after the start of the war in 2001. The Green Zone became an obstacle to ordinary urban life, causing a daily traffic nightmare that radiates throughout this sprawling city of more than 4 million people. In Kabul, it is felt as an alien presence, a source of deep resentment — and an indelible legacy of two decades of U.S. military intervention.','https://img.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/647KBJT2YUI6XDC6GLSHWQVVDM.jpg&w=1800','2021-03-25 00:00:00',4),(5,'Putin’s vaccine puzzle: Why the secrecy over his jab?','The Russian president has been photographed shirtless, atop a horse and while fishing. He has displayed his sporty side, from hockey to judo. And there is Putin the animal lover, posing with tigers, pups and a koala.\n\nBut one picture Putin refused to take? That of him getting vaccinated.\n\nThe Kremlin announced that the 68-year-old Putin received the first dose of one of the country’s three domestically made coronavirus vaccines Tuesday. It was an opportunity to try to boost Russians’ low trust in immunization. A poll from the independent Levada Center last month found that 62 percent were not willing to receive the Sputnik V vaccine, the first Russian-made coronavirus vaccine offered to the public.','https://www.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/5IUDS3UMTEI6XJZQDNHNSZLCLA.jpg&w=691','2021-03-25 00:00:00',4),(6,'Von Diaz’s Puerto Rican Recipes','Good morning. The journalist, historian and cookbook author Von Diaz brought together her essential Puerto Rican recipes for us this week, dishes that she calls foundational to her understanding of flavor, “a culinary mejunje, or mix, of Indigenous, African, Spanish and American ingredients and techniques.”\n\nHer essay on the subject is itself essential reading, and I think you’ll want to get into the recipes in your kitchen this week, building on her sazón and sofrito to make all manner of deliciousness.\n\nYou might start with pollo en fricasé, braised chicken thighs in a rich, oniony, tomato-based sauce with garlic, white wine and vinegar, set off by briny olives and capers. Or sancocho, the rustic stew you can make with root vegetables and just about any meat. Or, if you’re feeling celebratory, you might try your hand at pernil (above), the crackly-tender roast pork that is probably the best-known dish of the Puerto Rican diaspora.','https://static01.nyt.com/images/2021/03/24/dining/22Puertoricanrex3-pernil/merlin_184863150_fdc41442-45cd-4cc2-8585-f3e4dcc57972-jumbo.jpg','2021-03-25 00:00:00',2),(7,'The Brain of an Introvert','In the age of social media, networking and global never-ending communication, introverts are often viewed as rather inefficient. They are considered as people who would not happily express their opinion during the staff meetings or actively participate in brainstorming sessions. They are often considered to not be good at multitasking or be particularly charismatic. They are rarely at the center of attention at a party, and they often ignore their smartphones for hours in a row. These days, when we believe that big tasks require the active participation of large groups of people working together, being an introvert could come as a disadvantage.\n\nBut don’t discard introverts altogether: some of the most successful people in the world are introverts. Albert Einstein, Bill Gates, and even social media inventor Mark Zuckerberg are all self-confessed introverts. So how do these people who apparently lack some of the basic skills needed for a successful career manage to achieve so much? What makes the brain of an introvert so different and so special?','http://www.brainblogger.com/wp-content/uploads/The-Brain-of-an-Introvert-neuroscience-loneliness-771x516.jpg','2021-03-26 14:28:33',3),(12,'Drink your coffee like this to promote weight loss','If you are a coffee fanatic, then this new study will give you more reasons to stick to your cup of joe forever! Yes, according to a recent study now drinking a cup of coffee prior to workout can help in losing weight effectively and accelerate the fat burning process. Here’s all you need to know about this interesting study.\nThe findings of the study were published in the Journal of the International Society of Sports Nutrition. Scientists from the Department of Physiology of the University of Granada (UGR) have shown that caffeine (about 3 mg/kg, the equivalent of a strong coffee) taken half an hour before aerobic exercise significantly increased the rate of fat-burning. They also found that if the exercise is performed in the afternoon, the effects of the caffeine are more marked than in the morning.','https://www.criptotendencias.com/wp-content/uploads/2020/07/Cadena-de-suministro-del-caf%C3%A9-colombiano-de-la-marca-1850-Coffee-ser%C3%A1-rastreada-utilizando-la-tecnolog%C3%ADa-blockchain-de-IBM.jpg','2021-03-26 16:44:46',1);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-26 19:23:32
