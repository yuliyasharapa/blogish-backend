const express = require('express');
const cors = require('cors');

const postsRouter = require('./routes/posts.js');

//server set up
const app = express();
app.use(express.json());
app.use(cors());

const PORT = process.env.PORT || 5000;


//routes set up
app.use('/', postsRouter);


app.listen(PORT, ()=>{
    console.log(`The server has started on port ${PORT}`);
});



