const express = require('express');
const router = express.Router();
const postsController = require("../controllers/postsController");

router.get('/', postsController.listAll);
router.get('/:id', postsController.getById);
router.patch('/:id', postsController.edit);
router.delete('/:id', postsController.remove);
router.post('/', postsController.create);

module.exports = router;